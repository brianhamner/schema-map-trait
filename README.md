Laravel 4 Schema Mapping Trait


This trait will allow any database schema to define a form automatically.
Uses bootstrap html

I usually just include this in one of my autoload folders. 

Instructions:



1) Add the trait to any model as normal (ex. softdelete) :



		use  {path to file}\SchemaMappingTrait;

		class MyClass extends Eloquent{

			use SchemaMappingTrait;

		}



2) Call it in your View

	{{  {your model}::form( $url ='' , $values = NULL, $method = 'post', $files = false) }}

	$url = the url the form posts to. leave blank to post to itself
	
	$values = an array of values (used for displaying current info for update forms) 
			  for example if you were using this trait on your user model to create a user update form:  {{ User::form('/edit/user', $user->toArray() ) }}
			  leave blank for create forms

	$method = http form submit method 

	$files = boolean, set to true if the form will have a file upload (sets enctype)
